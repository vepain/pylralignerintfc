# -*- coding=utf-8 -*-

"""Just for module."""

from __future__ import division

from pathlib import Path


# ============================================================================ #
#                                     DATA                                     #
# ============================================================================ #
READS_FILE_PATH = Path(__file__).parent / 'data/DIS_5000_LR.fasta'
READS_FILE_STR = str(READS_FILE_PATH)

GENOME_FILE_PATH = Path(__file__).parent / 'data/DIS_5000.fasta'
GENOME_FILE_STR = str(GENOME_FILE_PATH)
