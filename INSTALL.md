Install
=======

Distant
-------

```bash
pip install git+https://gitlab.com/vepain/lralignerintfc@TAG
```

or (to be sure which python version you use)
```bash
python3.X -m pip install git+https://gitlab.com/vepain/lralignerintfc@TAG
```
where `TAG` is the tag version.

Local
-----

For the latest commit:
```bash
git clone https://gitlab.com/vepain/lralignerintfc.git
cd lralignerintfc
python3.X -m pip install .
```

So that when you want to update the package, you just to type in the git directory:
```bash
git pull
python3.X -m pip install . --upgrade
```
