# -*- coding: utf-8 -*-

"""LoReAs Alignment classes file.

AUTHOR: victor.epain@laposte.net
"""

from __future__ import annotations, division

from abc import abstractmethod
from subprocess import run as sp_run
from typing import Union

from lralignerintfc.aligner.aligner_abstract import AlignmentAbstract as AlAbs
from lralignerintfc.lib.paf_lib import PAFlib
from lralignerintfc.utils.path_utils import Path, rm


# ============================================================================ #
#                                   CONSTANTS                                  #
# ============================================================================ #
ONT = 'nanopore'
PB = 'pacbio'

# ============================================================================ #
#                                    OPTIONS                                   #
# ============================================================================ #

# ---------------------------------------------------------------------------- #
#                                    Preset                                    #
# ---------------------------------------------------------------------------- #
AVA_ONT = 'ava-ont'
AVA_PB = 'ava-pb'

MAP_ONT = 'map-ont'
MAP_PB = 'map-pb'


# ============================================================================ #
#                         ALIGNMENT CLASS FOR MINIMAP2                         #
# ============================================================================ #

class RootMinimap2(AlAbs):
    """Concrete super class for PAF file format."""

    ALIGNER: str = 'minimap2'

    _command: str = 'minimap2'

    # pylint: disable=too-many-arguments
    def __init__(self, query_file: Union[str, Path],
                 sequencer: str = ONT,
                 paf_filename: str = None,
                 align_dir: Union[str, Path] = AlAbs._ALIGN_DIR_DEFAULT,
                 log_filename: str = AlAbs._LOG_NAME_DEFAULT,
                 options: str = None):
        """Initializer.

        Parameters
        ----------
        query_file : Union[str, Path]
            The path of the query sequence(s)
        sequencer : str, optional
            The sequencing technology name, by default 'nanopore'
        """
        super().__init__(align_dir=align_dir,
                         log_filename=log_filename,
                         options=options)
        # inputs
        self._query_file = Path(query_file)
        self._sequencer = sequencer
        # ouputs
        # OPTIMIZE: separate output files
        self._paf = (
            self._align_dir / paf_filename if paf_filename is not None
            else self._set_paf_filepath()
        )

    def sort_idrs_nbres(self):
        """Sort by q_idr, then r_idr, finally by number of residue matches."""
        paf_out = self.paf()
        tmp_paf = self._align_dir / 'tmp_paf'
        paf_out.rename(tmp_paf)
        qidr_col = PAFlib.FIELDS_COL[PAFlib.Q_IDR] + 1
        ridr_col = PAFlib.FIELDS_COL[PAFlib.R_IDR] + 1
        nbres_col = PAFlib.FIELDS_COL[PAFlib.NB_RES] + 1
        sp_run(
            'sort'
            f' -k{qidr_col},{qidr_col}'
            f' -k{ridr_col},{ridr_col}'
            f' -k{nbres_col}nr,{nbres_col}'
            f' {tmp_paf} > {paf_out}',
            shell=True, check=True,
        )
        rm(tmp_paf)

    def is_done(self) -> bool:
        """Return True if the alignment was already done."""
        return self._paf.exists()

    # Getter
    # ------

    def paf(self) -> Path:
        """Return the resulting output."""
        return self._paf

    # Protected methods
    # -----------------

    @abstractmethod
    def _set_paf_filepath(self) -> Path:
        """Set output file(s) attribute(s)."""
        raise NotImplementedError


class AlignmentMinimap2(RootMinimap2):
    """Concrete Alignment class for PAF file format."""

    # pylint: disable=too-many-arguments
    def __init__(self, query_file: Union[str, Path],
                 sequencer: str = ONT,
                 paf_filename: str = None,
                 align_dir: Union[str, Path] = AlAbs._ALIGN_DIR_DEFAULT,
                 log_filename: str = AlAbs._LOG_NAME_DEFAULT,
                 options: str = None):
        """Initializer."""
        # OPTIMIZE: composition ReadsFileViewFasta
        # OPTIMIZE: separate input files
        super().__init__(query_file, sequencer=sequencer,
                         paf_filename=paf_filename,
                         align_dir=align_dir, log_filename=log_filename,
                         options=options)

    def run(self):
        """Run the aligner."""
        bash_cmd = (
            f'{super()._command}'
            f' -x {AVA_ONT if self._sequencer == ONT else AVA_PB}'
            f' {self._query_file} {self._query_file}'
            f' {self._options}'
            f' > {self._paf} 2> {self._log}'
        )
        sp_run(bash_cmd, shell=True, check=True)

    # Protected methods
    # -----------------

    def _set_paf_filepath(self) -> Path:
        """Set output file(s) attribute(s)."""
        # pylint: disable=old-division
        basename = self._query_file.stem
        return self._align_dir / f'{basename}_ava.{PAFlib.FORMAT_EXT}'


class MapperMinimap2(RootMinimap2):
    """Concrete Mapper class for PAF file format."""

    # pylint: disable=too-many-arguments
    def __init__(self, query_file: Union[str, Path],
                 target_file: Union[str, Path],
                 paf_filename: str = None,
                 sequencer: str = ONT,
                 align_dir: Union[str, Path] = AlAbs._ALIGN_DIR_DEFAULT,
                 log_filename: str = AlAbs._LOG_NAME_DEFAULT,
                 options: str = None):
        """Initializer."""
        self._target_file = Path(target_file)
        super().__init__(query_file, sequencer=sequencer,
                         paf_filename=paf_filename,
                         align_dir=align_dir, log_filename=log_filename,
                         options=options)

    def run(self):
        """Run the aligner."""
        bash_cmd = (
            f'{super()._command}'
            f' -x {MAP_ONT if self._sequencer == ONT else MAP_PB}'
            f' {self._target_file} {self._query_file}'
            f' {self._options}'
            f' > {self._paf} 2> {self._log}'
        )
        sp_run(bash_cmd, shell=True, check=True)

    # Protected methods
    # -----------------

    def _set_paf_filepath(self) -> Path:
        """Set output file(s) attribute(s)."""
        basename_q = self._query_file.stem
        basename_t = self._target_file.stem
        # pylint: disable=old-division
        return (
            self._align_dir
            / f'{basename_q}_map_{basename_t}.{PAFlib.FORMAT_EXT}'
        )
