# -*- coding: utf-8 -*-

"""LoReAs Alignment classes file.

AUTHOR: victor.epain@laposte.net
"""

from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Union

from lralignerintfc.utils.path_utils import Path


# ============================================================================ #
#                           ALIGNMENT ABSTRACT CLASS                           #
# ============================================================================ #

class AlignmentAbstract(ABC):
    """Abstract class Alignment class."""

    ALIGNER: str
    _ALIGN_DIR_DEFAULT: str = './alignments'
    _LOG_NAME_DEFAULT: str = 'aligner.log'

    _command: str

    def __init__(self, align_dir: Union[str, Path] = _ALIGN_DIR_DEFAULT,
                 log_filename: str = _LOG_NAME_DEFAULT,
                 options: str = None):
        """The Initializer."""
        # Alignments directory ---
        self._align_dir = Path(align_dir)
        self._align_dir.mkdir(exist_ok=True)
        # Log file ---
        self._log = self._align_dir / log_filename
        # Options ---
        self._options: str = options if options is not None else ''

    @classmethod
    def set_command(cls, command: Union[str, Path]):
        """Set command path for the aligner."""
        cls._command = str(command)

    @abstractmethod
    def run(self):
        """Run the aligner."""
        raise NotImplementedError

    def log_to_str(self) -> str:
        """Return the output of alignment program."""
        if self._log.exists():
            str_log = (
                f'{self.ALIGNER} output:\n'
                '```\n'
            )
            with open(self._log, 'r') as log_in:
                str_log += ''.join(log_in.readlines())
            return str_log + '```\n'
        return 'The Previous run did not produce a log file.\n'

    @abstractmethod
    def is_done(self) -> bool:
        """Return True if the alignment was already done."""
        raise NotImplementedError

    # Getter
    # ------

    def align_dir(self) -> Path:
        """Return the alignment directory."""
        return self._align_dir

    def log(self) -> Path:
        """Return the log file path."""
        return self._log
