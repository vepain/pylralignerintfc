# -*- coding=utf-8 -*-

"""Test aligner_minimap2 part."""

# pylint: disable=redefined-outer-name

from typing import Generator

import pytest

from lralignerintfc.aligner.aligner_minimap2 import AlignmentMinimap2
from lralignerintfc.utils.path_utils import rm
from tests import READS_FILE_PATH, READS_FILE_STR


# ============================================================================ #
#                                   ALIGNMENT                                  #
# ============================================================================ #

@pytest.fixture
def aligns() -> Generator[AlignmentMinimap2, None, None]:
    """Instanciate a AlignmentMinimap2 object."""
    # Arrange
    object_aligns = AlignmentMinimap2(READS_FILE_PATH)
    yield object_aligns
    # Cleanup
    # REMOVE outputs


def test_init_aligns():
    """Test init."""
    aligns = AlignmentMinimap2(READS_FILE_PATH)
    aligns_str = AlignmentMinimap2(READS_FILE_STR)
    assert aligns._query_file == aligns_str._query_file
    # remove directory
    rm(aligns.align_dir())
