# -*- coding=utf-8 -*-

"""Library for PAF format file.

AUTHOR: victor.epain@laposte.net
"""

from typing import Any, Dict, Optional, Sequence, Tuple

from lralignerintfc.lib.align_lib import AlignLibAbstract
from lralignerintfc.lib.reads_lib import FORWARD_INT, REVERSE_INT


# ============================================================================ #
#                                  PAF FORMAT                                  #
# ============================================================================ #

class PAFlib(AlignLibAbstract):
    """Data Class for PAF format file."""

    # File
    # ----

    FORMAT_EXT: str = 'paf'
    FILE_SEP: Optional[str] = None  # pylint: disable=unsubscriptable-object

    # Fields
    # ------

    Q_START = 'qstart'
    R_START = 'rstart'

    Q_END = 'qend'
    R_END = 'rend'

    STRAND = 'strand'
    NB_RES = 'nbres'
    ALBLOCK_LEN = 'alblocklen'
    MAPQ = 'mapq'

    FIELDS_COL: Dict[str, int] = {
        AlignLibAbstract.Q_IDR: 0,
        AlignLibAbstract.Q_LEN: 1,
        Q_START: 2,
        Q_END: 3,
        STRAND: 4,
        AlignLibAbstract.R_IDR: 5,
        AlignLibAbstract.R_LEN: 6,
        R_START: 7,
        R_END: 8,
        NB_RES: 9,
        ALBLOCK_LEN: 10,
        MAPQ: 11,
    }

    COLFIELD_TYPE: Tuple[Tuple[str, type], ...] = (
        (AlignLibAbstract.Q_IDR, str),
        (AlignLibAbstract.Q_LEN, int),
        (Q_START, int),
        (Q_END, int),
        (STRAND, str),
        (AlignLibAbstract.R_IDR, str),
        (AlignLibAbstract.R_LEN, int),
        (R_START, int),
        (R_END, int),
        (NB_RES, int),
        (ALBLOCK_LEN, int),
        (MAPQ, int),
    )

    # Codes
    # -----

    FORWARD_PAF: str = '+'
    REVERSE_PAF: str = '-'

    STRAND_PAF_INT = {
        FORWARD_PAF: FORWARD_INT,
        REVERSE_PAF: REVERSE_INT,
    }

    @classmethod
    def get_paf_fields(cls, fields: Sequence[str], field: str) -> Any:
        """Return the type formated field in fields."""
        col = cls.FIELDS_COL[field]
        return cls.COLFIELD_TYPE[col][1](fields[col])
