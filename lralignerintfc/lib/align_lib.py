# -*- coding=utf-8 -*-

"""Alignments file abstract library.

AUTHOR: victor.epain@laposte.net
"""

from abc import ABC
from typing import Dict, Optional, Tuple


# ============================================================================ #
#                               ALIGNMENTS FORMAT                              #
# ============================================================================ #

class AlignLibAbstract(ABC):
    """Abstract class for alignments."""

    # File
    # ----

    FORMAT_EXT: str
    FILE_SEP: Optional[str]  # pylint: disable=unsubscriptable-object

    # Fileds
    # ------

    Q_IDR = 'qid'
    R_IDR = 'rid'

    Q_LEN = 'qlen'
    R_LEN = 'rlen'

    FIELDS_COL: Dict[str, int]

    COLFIELD_TYPE: Tuple[Tuple[str, type], ...]

    # Codes
    # -----

    # Alignment type integer value:
    #   - 0: overlap
    #   - 1: subseq
    #   - 2: other
    ALTYPE_OVERLAP: int = 0
    ALTYPE_SUBSEQ: int = 1
    ALTYPE_OTHER: int = 2

    # Alignment way integer value:
    #   - 0: q-r way or q contains r
    #   - 1: r-q way or r contains q
    #   - 2: unknow
    ALWAY_QR: int = 0
    ALWAY_RQ: int = 1
    ALWAY_OTHER: int = 2
