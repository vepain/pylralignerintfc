# -*- coding: utf-8 -*-

"""Path utils file."""

from pathlib import Path


# ============================================================================ #
#                                  PATH UTILS                                  #
# ============================================================================ #

def is_empty(path: Path) -> bool:
    """Return True if file / directory is empty."""
    if path.is_file():
        return not path.stat().st_size
    return not list(path.glob('*'))


def rm(path: Path):
    """Remove the file or the directory if exists."""
    if not path.exists():
        raise FileNotFoundError(path)
    if path.is_file():
        path.unlink()
    else:
        for child in path.glob('*'):
            rm(child)
        path.rmdir()


def create_file(path: Path):
    """Create an empty file."""
    if path.exists():
        raise FileExistsError(path)
    if not path.parent.exists():
        raise FileNotFoundError(path)
    with open(path, 'w'):
        pass
