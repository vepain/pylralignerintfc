# -*- coding=utf-8 -*-

"""The abstract overlaps extractor file.

Alignment types
===============

    - overlap:
        ----------> q
            ----------> r
    - subseq:
        --------------> q
            -------> r

AUTHOR: victor.epain@laposte.net
"""

from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Any, Generator, Iterable, Tuple, Type

from lralignerintfc.lib.align_lib import AlignLibAbstract
from lralignerintfc.lib.reads_lib import IdT, OrT


# ============================================================================ #
#                     ALIGNMENTS EXTRACTOR ABSTRACT CLASS                      #
# ============================================================================ #

class AlignmentViewAbstract(ABC):
    """Abstract class OverlapViewAbstract."""

    _FILE_FORMAT: Type[AlignLibAbstract] = AlignLibAbstract

    def __init__(self, alline: str):
        """The Initializer."""
        self._fields = self._to_ovlist(alline)
        # attributes after identifying and formatting alignment
        self._reads_ors: Tuple[OrT, OrT]
        self._altype: int
        self._alway: int

    @classmethod
    def _to_ovlist(cls, ovline: str) -> Tuple[Any, ...]:
        """Return a list containing overlap informations in `ovline`."""
        return tuple(
            cls._FILE_FORMAT.COLFIELD_TYPE[col][1](info)
            if col < len(cls._FILE_FORMAT.COLFIELD_TYPE) else str(info)
            for col, info in enumerate(ovline.split(cls._FILE_FORMAT.FILE_SEP))
        )

    @abstractmethod
    def identifie_format(self) -> Tuple[int, int, int, int]:
        """Determine the alignment type."""
        raise NotImplementedError

    @abstractmethod
    def q_allen(self) -> int:
        """Return alignment length on read q."""
        raise NotImplementedError

    @abstractmethod
    def r_allen(self) -> int:
        """Return alignment length on read r."""
        raise NotImplementedError

    def get_field(self, field: str) -> Any:
        """Return the `field` correspondance."""
        return self._fields[self._FILE_FORMAT.FIELDS_COL[field]]

    def get_fields(self,
                   fields: Iterable[str]) -> Generator[Any, None, None]:
        """Return a generator on `fields`."""
        return (
            self._fields[self._FILE_FORMAT.FIELDS_COL[field]]
            for field in fields
        )

    def q_idr(self) -> IdT:
        """Return the first read id in (q, r) overlap."""
        return self._fields[
            self._FILE_FORMAT.FIELDS_COL[self._FILE_FORMAT.Q_IDR]
        ]

    def r_idr(self) -> IdT:
        """Return the second read id in (q, r) overlap."""
        return self._fields[
            self._FILE_FORMAT.FIELDS_COL[self._FILE_FORMAT.R_IDR]
        ]

    def q_len(self) -> int:
        """Return the q read length."""
        return self._fields[
            self._FILE_FORMAT.FIELDS_COL[self._FILE_FORMAT.Q_LEN]
        ]

    def r_len(self) -> int:
        """Return the r read length."""
        return self._fields[
            self._FILE_FORMAT.FIELDS_COL[self._FILE_FORMAT.R_LEN]
        ]

    def q_idr_or(self) -> Tuple[IdT, OrT]:
        """Return the first read id in (q, r) overlap and its orientation."""
        return (
            self._fields[self._FILE_FORMAT.FIELDS_COL[self._FILE_FORMAT.Q_IDR]],
            self._reads_ors[0],
        )

    def r_idr_or(self) -> Tuple[IdT, OrT]:
        """Return the second read id in (q, r) overlap and its orientation."""
        return (
            self._fields[self._FILE_FORMAT.FIELDS_COL[self._FILE_FORMAT.R_IDR]],
            self._reads_ors[1],
        )

    def is_overlap(self) -> bool:
        """Return True if alignment is an overlap type."""
        return self._altype == self._FILE_FORMAT.ALTYPE_OVERLAP

    def is_subseq(self) -> bool:
        """Return True if alignment is a subseq type."""
        return self._altype == self._FILE_FORMAT.ALTYPE_SUBSEQ

    def is_q_r_way(self) -> bool:
        """Return True if the alignment is q-r way."""
        return self._alway == self._FILE_FORMAT.ALWAY_QR

    def is_r_q_way(self) -> bool:
        """Return True if the alignment is q-r way."""
        return self._alway == self._FILE_FORMAT.ALWAY_RQ
