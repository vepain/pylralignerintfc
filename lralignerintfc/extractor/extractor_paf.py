# -*- coding=utf-8 -*-

"""The LoReAs overlap view classes file.

AUTHOR: victor.epain@laposte.net
"""

from typing import Tuple, Type

from lralignerintfc.extractor.extractor_abstract import (
    AlignmentViewAbstract,
)
from lralignerintfc.lib.paf_lib import PAFlib
from lralignerintfc.lib.reads_lib import FORWARD_INT, REVERSE_INT

# ============================================================================ #
#                        ALIGNMENT VIEW CLASS FOR PAF                          #
# ============================================================================ #


class AlignmentViewPAF(AlignmentViewAbstract):
    """The Overlap View for PAF file format concrete class."""

    _FILE_FORMAT: Type[PAFlib] = PAFlib

    EPSILON_DEF = 100

    def __init__(self, alline: str, epsilon: int = EPSILON_DEF):
        """The Initializer."""
        super().__init__(alline)
        self._epsilon = epsilon

    def identifie_format(self) -> Tuple[int, int, int, int]:
        """Determine the alignment type."""
        # -------------------------------------------------------------------- #
        #
        #   q_1             q_2           r_1             r_2
        #   ---|-----------|---> q        ---|-----------|---> r
        #
        # -------------------------------------------------------------------- #
        q_1 = self.get_field(self._FILE_FORMAT.Q_START)
        q_2 = self.q_len() - self.get_field(self._FILE_FORMAT.Q_END)
        r_1 = self.get_field(self._FILE_FORMAT.R_START)
        r_2 = self.r_len() - self.get_field(self._FILE_FORMAT.R_END)
        # -------------------------------------------------------------------- #
        # r strand
        # -------------------------------------------------------------------- #
        q_or = FORWARD_INT
        if not self._FILE_FORMAT.STRAND_PAF_INT[
            self.get_field(self._FILE_FORMAT.STRAND)
        ]:  # 0 = '+'
            r_or = FORWARD_INT
        else:  # 1 = '-'
            r_or = REVERSE_INT
        self._reads_ors = (q_or, r_or)
        # -------------------------------------------------------------------- #
        # Alignment way
        # -------------------------------------------------------------------- #
        if r_1 + r_2 < self._epsilon and r_1 <= q_1 and r_2 <= q_2:
            self._altype = self._FILE_FORMAT.ALTYPE_SUBSEQ
            self._alway = self._FILE_FORMAT.ALWAY_QR
        elif q_1 + q_2 < self._epsilon and q_1 <= r_1 and q_2 <= r_2:
            self._altype = self._FILE_FORMAT.ALTYPE_SUBSEQ
            self._alway = self._FILE_FORMAT.ALWAY_RQ
        elif (r_1 < self._epsilon and q_2 < self._epsilon
                and q_1 > r_1 and q_2 < r_2):
            self._altype = self._FILE_FORMAT.ALTYPE_OVERLAP
            self._alway = self._FILE_FORMAT.ALWAY_QR
        elif (r_2 < self._epsilon and q_1 < self._epsilon
                and q_1 < r_1 and q_2 > r_2):
            self._altype = self._FILE_FORMAT.ALTYPE_OVERLAP
            self._alway = self._FILE_FORMAT.ALWAY_RQ
        else:
            self._altype = self._FILE_FORMAT.ALTYPE_OTHER
            self._alway = self._FILE_FORMAT.ALWAY_OTHER
        # Returns alignment on reads junctions
        return q_1, q_2, r_1, r_2

    def q_allen(self) -> int:
        """Return alignment length on read q."""
        return (
            self._fields[
                self._FILE_FORMAT.FIELDS_COL[self._FILE_FORMAT.Q_END]
            ]
            - self._fields[
                self._FILE_FORMAT.FIELDS_COL[self._FILE_FORMAT.Q_START]
            ]
        )

    def r_allen(self) -> int:
        """Return alignment length on read r."""
        return (
            self._fields[
                self._FILE_FORMAT.FIELDS_COL[self._FILE_FORMAT.R_END]
            ]
            - self._fields[
                self._FILE_FORMAT.FIELDS_COL[self._FILE_FORMAT.R_START]
            ]
        )

    def q_end(self) -> int:
        """Return the length of read q."""
        return self._fields[
            self._FILE_FORMAT.FIELDS_COL[self._FILE_FORMAT.Q_END]
        ]

    def r_end(self) -> int:
        """Return the length of read r."""
        return self._fields[
            self._FILE_FORMAT.FIELDS_COL[self._FILE_FORMAT.R_END]
        ]
