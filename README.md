Simple long read aligner package
================================

Installation
------------

### Distant

```bash
pip install -e git+https://gitlab.com/vepain/lralignerintfc@TAG
```

or (to be sure which python version you use)
```bash
python3.X -m pip install -e git+https://gitlab.com/vepain/lralignerintfc@TAG
```
where `TAG` is the tag version.

### Local

For the latest commit:
```bash
git clone https://gitlab.com/vepain/lralignerintfc.git
cd lralignerintfc
python3.X -m pip install -e .
```

So that when you want to update the package, you just to type in the git directory:
```bash
git pull
```

Usage
-----


Next
----

Dev
---

### Version

Change the version in following files:
- `setup.cfg`
- `docs/source/conf.py`